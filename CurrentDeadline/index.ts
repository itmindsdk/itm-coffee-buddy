import { AzureFunction, Context, HttpRequest } from "@azure/functions";
import { WebClient } from "@slack/web-api";
import { Channel, Group, Interval } from "../@types/CoffeeGroups";
import { DocTypes } from "../@types/Cosmos";
import { Conversation } from "../@types/Slack";
import { CosmosDB } from "../_cosmos";
import { getChannelBotIsMemberOf } from "../_slack/getChannelBotIsMemberOf";

const httpTrigger: AzureFunction = async function (
  context: Context,
  req: HttpRequest
): Promise<void> {
  context.log("HTTP trigger function CurrentDeadline processed a request.");
  let group: Group;
  let channel: Channel<Interval>;

  const params = new URL(req.url + "?" + req.body).searchParams;

  const token = process.env.SLACK_TOKEN;
  const web = new WebClient(token);

  const allConversations = await getChannelBotIsMemberOf();

  const container = await new CosmosDB().initialize();

  await Promise.all(
    allConversations.map(convo => {
      container
        .retype(DocTypes.CoffeeGroups)
        .where({
          key: "channelId",
          val: convo.id
        })
        .first()
        .then((ch: Channel<Interval>) => {
          switch (params.get("channel_name")) {
            case "privategroup":
              group = ch?.currentGroups.find(
                group => group.channelId === params.get("channel_id")
              );
              if (!group) return;
              channel = ch;
              break;
            default:
              group = ch?.currentGroups.find(group =>
                group.memberIds.includes(params.get("user_id"))
              );
              if (!group) return;
              channel = ch;
              break;
          }
        });
    })
  );

  if (group.groupCreatedTs) {
    web.chat.update({
      channel: group.channelId,
      ts: group.groupCreatedTs,
      token: token,
      text: channel.settings.reminderMessageTemplate.replace(
        "{{1}}",
        channel.currentDeadline.toString()
      ),
      blocks: [
        {
          type: "section",
          text: {
            type: "mrkdwn",
            text: channel.settings.reminderMessageTemplate.replace(
              "{{1}}",
              new Date(channel.currentDeadline).toDateString()
            )
          }
        }
      ]
    });
  } else {
    web.chat.postEphemeral({
      user: params.get("user_id"),
      text: channel.settings.reminderMessageTemplate.replace(
        "{{1}}",
        new Date(channel.currentDeadline).toDateString()
      ),
      channel: params.get("channel_id")
    });
  }

  context.res = {
    status: 200
  };
};

export default httpTrigger;
