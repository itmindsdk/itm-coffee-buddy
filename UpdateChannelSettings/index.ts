import { AzureFunction, Context, HttpRequest } from "@azure/functions";
import { WebClient } from "@slack/web-api";
import { ChannelVM } from "../@types/CoffeeGroups";
import { DocTypes } from "../@types/Cosmos";
import { Conversation } from "../@types/Slack";
import { CosmosDB } from "../_cosmos";
import { getChannelBotIsMemberOf } from "../_slack/getChannelBotIsMemberOf";

const httpTrigger: AzureFunction = async function (
  context: Context,
  req: HttpRequest
): Promise<void> {
  context.log(
    "HTTP trigger function UpdatechannelSettings processed a request."
  );
  console.log(req);
  let newConvo;
  let channel;

  try {
    const allConversations = await getChannelBotIsMemberOf();

    const container = await new CosmosDB().initialize();

    const convo = await allConversations.find(
      convo => convo.id === req.body.channelId
    );

    if (convo) {
      channel = await container
        .retype(DocTypes.CoffeeGroups)
        .where({
          key: "channelId",
          val: convo.id
        })
        .first();
      newConvo = channel;
      newConvo.settings = req.body.settings;
      await container.upsertInDB(channel);

      context.res = {
        status: 200,
        body: new ChannelVM(
          channel.channelName,
          channel.channelId,
          channel.settings
        )
      };
    } else {
      context.res = {
        status: 400,
        body: `Bad Request`
      };
    }
  } catch (error) {
    context.res = {
      status: 503,
      body: `Service Unavailable: ${error}`
    };
  }
};

export default httpTrigger;
