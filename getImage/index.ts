import { AzureFunction, Context, HttpRequest } from "@azure/functions"

const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
    context.log('HTTP trigger function GetImage processed a request.');

    const fetch = require("node-fetch");
    const token = process.env.SLACK_TOKEN;
    const imageUrl = req.query.imageUrl;
    const errorBody = {
        "allowed http methods for this endpoint": "GET",
        "query parameter": "imageUrl",
        "error specification": "Whatever element specified by the 'imageUrl' could't be found",
        "endpoint purpose": "This an endpoint made to extract a file from It Mind's Slack workspace using an url reference to files.slack.com",
        "possible solutions": {
            "1": "Make sure that the IT Minds coffee buddy have access to the file you are looking for",
            "2": "Make sure you are passing the rigth url with the imageUrl in this call. See the syntax below"
        },
        "syntax for imageUrl if using the url_private": "https://files.slack.com/files-pri/<teamId>-<fileId>/<fileName on slack>",
        "how to find": "To find all valid urls to pass as the imageUrl on this endpoint, see the documentation on slack: https://api.slack.com/methods/files.info",
        "allowed urls on slack": "All of the urls that require authentication. See the following link for info: https://api.slack.com/types/file#authentication"
    }

    if (!imageUrl) {
        context.res = {
            headers: {
                'Content-Type': 'application/json'
            },
            status: 400,
            body: errorBody
        }
        return;
    }

    await fetch(imageUrl, {
        method: "GET",
        redirect: 'follow',
        follow: 20,
        headers: {
            'Authorization': `Bearer ${token}`
        }
    }).then((response: Response) => response.blob()
    ).then((data: Blob) =>
        data.arrayBuffer().then(arrayBuffer => ({
            arrayBuffer,
            blob: data
        }))
    ).then((data: { arrayBuffer: ArrayBuffer, blob: Blob }) => {
        const buffer = Buffer.from(data.arrayBuffer);
        context.res = {
            headers: {
                "Content-Type": data.blob.type
            },
            body: buffer
        }
    }).catch(() =>
        context.res = {
            headers: {
                'Content-Type': 'application/json'
            },
            status: 404,
            body: errorBody
        }
    )
};

export default httpTrigger;