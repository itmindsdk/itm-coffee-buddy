import { AzureFunction, Context, HttpRequest } from "@azure/functions"
import { WebClient } from "@slack/web-api";
import { Channel, ImageCollageVM, Interval } from "../@types/CoffeeGroups";
import { DocTypes } from "../@types/Cosmos";
import { FilesInfo } from "../@types/Slack";
import { CosmosDB } from "../_cosmos";
import { getChannelBotIsMemberOf } from "../_slack/getChannelBotIsMemberOf";
import { retrieveFromArchives } from "./retrieveFromArchives";

const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
    context.log('HTTP trigger function ImageCollage processed a request.');

    const token = process.env.SLACK_TOKEN;
    const web = new WebClient(token);
    const allConversations = await getChannelBotIsMemberOf();
    const container = await new CosmosDB().initialize();

    let photoVMs: ImageCollageVM[] = [];
    let fetchPictures: number = parseInt(req.query.amountOfImages);
    if (!fetchPictures) fetchPictures = 8;
    
    await Promise.all(allConversations.map(async convo => {
        await container.retype(DocTypes.CoffeeGroups)
            .where({
                key: "channelId",
                val: convo.id
            }).first()
            .then(async (channel: Channel<Interval>) => {
                if (!channel) return;
                let imageVM: ImageCollageVM = {
                    channelName: channel.channelName,
                    channelId: channel.channelId,
                    photoUrls: [],
                    error: [],
                };
                let images: string[] = [];
                let tempImages: string[] = [];
                channel.currentGroups.map(group => {
                    tempImages.push(group?.photoUrl);
                })


                while (tempImages.length > 0 && images.length < fetchPictures) {
                    const group = Math.floor(Math.random() * (tempImages.length));
                    const photoId = tempImages[group]
                    if (photoId && !images.includes(photoId))
                        images.push(photoId);
                    tempImages.splice(group, 1);
                }

                await retrieveFromArchives(channel.channelId, fetchPictures - images.length)
                    .then(res => {
                        if (res) {
                            images = images.concat(res)
                        }
                    });

                await Promise.all(images.map(async (photoUrl) => {
                    await web.files.info({
                        token: token,
                        file: photoUrl
                    }).then((res: unknown) => {
                        const info = res as FilesInfo;
                        const url = info.file.url_private;
                        imageVM.photoUrls.push(`${url}`);
                    }).catch(error => {
                        imageVM.error.push({
                            deadline: channel.currentDeadline,
                            groupId: channel.currentGroups.find(group => group.photoUrl === photoUrl).channelId,
                            fileId: photoUrl,
                            error: `${error}`
                        })
                    });
                }))

                photoVMs.push(imageVM)
            })
    })).then(() =>
        context.res = {
            headers: {
                'Content-Type': 'application/json'
            },
            status: 200,
            body: photoVMs
        }
    )
};

export default httpTrigger;