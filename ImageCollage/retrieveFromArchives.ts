import { ChannelArchive } from "../@types/CoffeeGroups";
import { DocTypes } from "../@types/Cosmos";
import { CosmosDB } from "../_cosmos";

/**
 * 
 * @param channelId Id string to Identify the channel the Cosmos
 * @param amountToRetrieve The amount of images you'll want to retrieve from thee archives
 */
export const retrieveFromArchives = async (channelId: string, amountToRetrieve: number): Promise<string[]> => {

    const container = await new CosmosDB().initialize();
    let imageIds: string[] = [];

    await container.retype(DocTypes.GroupArchives)
        .where({
            key: "channelId",
            val: channelId
        }).readAllMatchedEntries()
        .then((archives: ChannelArchive[]) => {
            archives.shift();

            if (archives.length === 0) {
                imageIds = undefined;
                return imageIds;
            }
            
            while (amountToRetrieve > 0 && archives.length > 0) {
                const chosenArchive = Math.floor(Math.random() * (archives.length));
                
                const groups = archives[chosenArchive].groups;
                const group = Math.floor(Math.random() * (groups.length));

                const photoId = groups[group].photoUrl
                if (photoId && !imageIds.includes(photoId)) {
                    imageIds.push(photoId);
                    --amountToRetrieve;
                }
                archives[chosenArchive].groups.splice(group, 1);
                if (archives[chosenArchive].groups.length === 0) {
                    archives.splice(chosenArchive, 1);
                }
            }

        })


    return imageIds;
}