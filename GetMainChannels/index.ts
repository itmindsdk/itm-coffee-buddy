import { AzureFunction, Context, HttpRequest } from "@azure/functions";
import { WebClient } from "@slack/web-api";
import { ChannelVM } from "../@types/CoffeeGroups";
import { DocTypes } from "../@types/Cosmos";
import { Conversation } from "../@types/Slack";
import { CosmosDB } from "../_cosmos";
import { getChannelBotIsMemberOf } from "../_slack/getChannelBotIsMemberOf";

const httpTrigger: AzureFunction = async function (
  context: Context,
  req: HttpRequest
): Promise<void> {
  context.log("HTTP trigger function GetAllChannels processed a request.");
  var result;

  try {
    const allConversations = await getChannelBotIsMemberOf();

    const container = await new CosmosDB().initialize();

    const conversations = await Promise.all(
      allConversations.map(convo =>
        container
          .retype(DocTypes.CoffeeGroups)
          .where({
            key: "channelId",
            val: convo.id
          })
          .first()
      )
    );

    result = conversations
      .filter(convo => convo !== null)
      .map(convo => {
        return new ChannelVM(
          convo.channelName,
          convo.channelId,
          convo.settings
        );
      });
    context.res = {
      status: 200,
      body: result
    };
  } catch (error) {
    context.log.error(error);
    context.res = {
      status: 500,
      body: error
    };
  }
};

export default httpTrigger;
