export enum Interval {
    DAILY,
    WEEKLY,
    BIWEEKLY,
    MONTHLY
}

/// Interval ext.
export enum Weekly_Days {
    MON = "MON",
    TUES = "TUES",
    WED = "WED",
    THUR = "THUR",
    FRI = "FRI",
    SAT = "SAT",
    SUN = "SUN"
}

export type IntervalExt<T extends Interval> = T extends Interval.WEEKLY
    ? Weekly_Days
    : T extends Interval.BIWEEKLY
    ? Weekly_Days
    : number;

export type ChannelSettings<T extends Interval> = {
    individualGroupMessagesEnabled: boolean;
    individualGroupMessageTemplate: string;
    reminderMessageTemplate: string;
    channelMessageTemplate: string;
    channelReportTemplate: string;
    resumeCoffeeTemplate: string;
    resumeErrorMessageTemplate: string;
    onPauseMessageTemplate: string;
    alreadyOnPauseMessageTemplate: string;
    coffeeDoneTemplate: string;
    groupSize: number;
    interval: T;
    intervalExt: IntervalExt<T>;
};

export class ChannelVM<T extends Interval = Interval.BIWEEKLY> {
    constructor(channelName: string, channelId: string, settings: ChannelSettings<T>) {
        this.channelName = channelName, this.channelId = channelId, this.settings = settings;
    }

    channelId: string;
    channelName: string;
    settings: ChannelSettings<T>;
}