import {
  createContext,
  Dispatch,
  SetStateAction,
  useCallback,
  useEffect,
  useMemo,
  useReducer,
  useState
} from "react";
import { ChannelVM, Interval, Weekly_Days } from "../../@types/CoffeeGroups";
import { StatusCodes } from "../../@types/Network";
import { ChannelClient } from "../utils/ChannelClient";
import ListReducer, { ListReducerActionType } from "../utils/listReducer";

export type ChannelContextType = {
  channels: ChannelVM<Interval>[];
  loading: boolean;
  error: boolean;
  showResponse: boolean;
  setShowResponse: Dispatch<SetStateAction<boolean>>;
  serverResponse: string;
  setServerResponse: Dispatch<SetStateAction<string>>;
  hasServerResponded: boolean;
  setHasServerResponded: Dispatch<SetStateAction<boolean>>;

  getChannels: () => void;
  updateChannelSettings: (newConfig: ChannelVM<Interval>, oldConfig: ChannelVM<Interval>) => void;

  onChannelSelect: (e: any) => void;
  onToggleGroupMessage: (e: any) => void;
  onIntervalChange: (e: any) => void;
  onIntervalExtChange: (e: any) => void;
  onMessageSubmit: (e: any) => void;
  selectedChannel: ChannelVM<Interval>;
  updatingInterval: boolean;
  updatingIntervalExt: boolean;
  updatingMessage: boolean;
  toggelingGroupMsg: boolean;
};

const ChannelContext = createContext<ChannelContextType>(null);

export const useChannelContext = (): ChannelContextType => {
  const channelClient = useMemo(
    () => new ChannelClient(process.env.NEXT_PUBLIC_API),
    []
  );
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [selectedChannel, setSelectedChannel] = useState<ChannelVM<Interval>>(null);

  const [toggelingGroupMsg, setToggelingGroupMsg] = useState(false);
  const [updatingInterval, setUpdatingInterval] = useState(false);
  const [updatingIntervalExt, setUpdatingIntervalExt] = useState(false);
  const [updatingMessage, setUpdatingMessage] = useState(false);

  const [channels, channelDispatcher] = useReducer(
    ListReducer<ChannelVM<Interval>>("channelId"),
    []
  );

  const [hasServerResponded, setHasServerResponded] = useState(false);
  const [showResponse, setShowResponse] = useState(false);
  const [serverResponse, setServerResponse] = useState("");
  const onChannelSelect = useCallback(
    e => {
      setSelectedChannel(
        channels.find(channel => channel.channelId === e.target.id)
      );
    },
    [channels]
  );

  const onToggleGroupMessage = useCallback(
    e => {
      setToggelingGroupMsg(true);
      const oldChannelConfig = JSON.parse(JSON.stringify(selectedChannel));
      selectedChannel.settings.individualGroupMessagesEnabled = !selectedChannel
        .settings.individualGroupMessagesEnabled;
      updateChannelSettings(selectedChannel, oldChannelConfig);
    },
    [channels, selectedChannel]
  );

  const onIntervalChange = useCallback(
    e => {
      setUpdatingInterval(true);
      const oldChannelConfig = JSON.parse(JSON.stringify(selectedChannel));
      selectedChannel.settings.interval = parseInt(e.target.id);

      if (e.target.id == Interval.DAILY || e.target.id == Interval.MONTHLY)
        selectedChannel.settings.intervalExt = 15;

      if (e.target.id == Interval.WEEKLY || e.target.id == Interval.BIWEEKLY)
        selectedChannel.settings.intervalExt = Weekly_Days.MON;

      updateChannelSettings(selectedChannel, oldChannelConfig);
    },
    [channels, selectedChannel]
  );

  const onIntervalExtChange = useCallback(
    (e) => {
      setUpdatingInterval(true);
      const oldChannelConfig = JSON.parse(JSON.stringify(selectedChannel));
      if (
        selectedChannel.settings.interval == Interval.WEEKLY ||
        selectedChannel.settings.interval == Interval.BIWEEKLY
      ) {
        selectedChannel.settings.intervalExt = e.target.id;
      } else if (
        selectedChannel.settings.interval == Interval.DAILY ||
        selectedChannel.settings.interval == Interval.MONTHLY
      ) {
        selectedChannel.settings.intervalExt = parseInt(e.target.value);
      }
      updateChannelSettings(selectedChannel, oldChannelConfig);
    },
    [channels, selectedChannel]
  );

  const onMessageSubmit = useCallback(
    e => {
      setUpdatingMessage(true);
      const oldChannelConfig = JSON.parse(JSON.stringify(selectedChannel));
      selectedChannel.settings[e.target.id] = e.target.value;
      updateChannelSettings(selectedChannel, oldChannelConfig);
    },
    [channels, selectedChannel]
  );

  const getChannels = useCallback(() => {
    channelClient.get().then(
      viewModel => {
        channelDispatcher({
          type: ListReducerActionType.Reset,
          data: viewModel
        });
        setLoading(false);
      },
      err => {
        setError(true);
        setLoading(false);
        setHasServerResponded(true);
        setServerResponse(
          "Could not retrieve channels. Prøv igen lidt senere eller kontakt udviklere om problemet"
        );
      }
    ).finally(() => {
      setUpdatingInterval(false);
      setUpdatingIntervalExt(false);
      setUpdatingMessage(false);
      setToggelingGroupMsg(false);
      setLoading(false);
    });
  }, [channelClient]);

  const updateChannelSettings = useCallback(
    (newConfig: ChannelVM<Interval>, oldConfig: ChannelVM<Interval>) => {
      setLoading(true);
      channelClient.updateChannelSettings(newConfig).then(
        (result) => {
          setHasServerResponded(true);
          switch (result) {
            case StatusCodes.OK:
              setServerResponse("Kaffe botten er nu opdateret");
              setError(false);
              channelDispatcher({
                type: ListReducerActionType.Update,
                data: newConfig,
              });
              setSelectedChannel(newConfig)
              break;
            case StatusCodes.BAD_REQUEST:
              setServerResponse(
                "Der er sket en fejl. Din kanal er ikke i registret"
              );
              setError(true);
              setSelectedChannel(oldConfig)
              break;
            case StatusCodes.SERVICE_UNAVAILABLE:
              setServerResponse(
                "Databasen er midlertidigt nede.\nPrøv igen senere"
              );
              setError(true);
              setSelectedChannel(oldConfig)
              break;
            default:
          }
        },
        err => {
          setError(true);
          setServerResponse(
            "Der er desværre tekniske problemer. Prøv igen lidt senere eller kontakt udviklere om problemet"
          );
          setSelectedChannel(oldConfig)
        }
      ).finally(() => {
        setHasServerResponded(true);
        setLoading(false);

        setUpdatingInterval(false);
        setUpdatingIntervalExt(false);
        setUpdatingMessage(false);
        setToggelingGroupMsg(false);
      });
    },
    [channelClient]
  );

  useEffect(() => {
    getChannels();
  }, [getChannels]);

  useEffect(() => {
    if(!selectedChannel){
        setSelectedChannel(channels[0]);
    }
  }, [channels])

  return {
    channels,
    loading,
    error,
    getChannels,
    updateChannelSettings,

    onChannelSelect,
    selectedChannel,
    onToggleGroupMessage,
    toggelingGroupMsg,
    onIntervalChange,
    updatingInterval,
    onIntervalExtChange,
    updatingIntervalExt,
    onMessageSubmit,
    updatingMessage,

    showResponse,
    setShowResponse,
    serverResponse,
    setServerResponse,
    hasServerResponded,
    setHasServerResponded
  };
};

export default ChannelContext;
