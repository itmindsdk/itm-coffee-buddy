import { ChannelVM, Interval } from "../../@types/CoffeeGroups"


export class ChannelClient {
    baseurl: string;
    constructor(baseurl : string){
        this.baseurl = baseurl;
    }

    updateChannelSettings = async (channelVm : ChannelVM<Interval>) => {
        var result
        await fetch(this.baseurl+'/api/UpdateChannelSettings',{
            method: 'POST',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers : {
                'Content-Type': 'application/json'
            },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            body: JSON.stringify(channelVm)
        }).then(response => {
            result = response.status
        })
        return result
    }

    get = async () : Promise<ChannelVM<Interval>[]> => {
        return fetch(this.baseurl + '/api/GetMainChannels')
            .then((response) => { return response.json(); })
    }
}