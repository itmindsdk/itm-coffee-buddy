import React, { FunctionComponent, useContext } from "react";
import ChannelContext from "../../contexts/ChannelContext";
import { GroupHeader } from "../GroupHeader/GroupHeader";
import { IntervalPicker } from "../IntervalPicker/IntervalPicker";
import { MessageConfig } from "../MessageConfig/MessageConfig";
import { MessageToggle } from "../MessageToggle/MessageToggle";
import ServerResponse from "../ServerResponse/ServerResponse";
import { TopNav } from "../TopNav/TopNav";
import styles from "./ContentComponent.module.css";

type Props = {};

export const ContentComponent: FunctionComponent<Props> = ({}) => {
  const { selectedChannel } = useContext(ChannelContext);

  return (
    <div className={styles.contentContainer}>
      <TopNav />
      <div className={styles.content}>
        {selectedChannel && <GroupHeader/>}
        {selectedChannel && <MessageToggle/>}
        {selectedChannel && <IntervalPicker />}
        {selectedChannel && <MessageConfig />}
        <ServerResponse/>
      </div>
    </div>
  );
};
