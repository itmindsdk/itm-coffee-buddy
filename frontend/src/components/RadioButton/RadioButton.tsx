import { FunctionComponent, useContext } from "react";
import ChannelContext from "../../contexts/ChannelContext";
import styles from "./RadioButton.module.css";

type Props = {
  id?: any;
  label?: string;
  value: boolean;
  disabled?: boolean;
  onClick: any;
};

export const RadioButton: FunctionComponent<Props> = ({
  id,
  label,
  value,
  disabled,
  onClick,
}) => {
  const { loading, hasServerResponded } = useContext(ChannelContext);
  return (
    <>
      <label id={id} className={styles.container}>
        {label && <div>{label}</div>}
        <input
          className={styles.checkmark}
          id={id}
          type="checkbox"
          checked={value}
          onClick={(e) => onClick(e)}
          disabled={disabled}
          readOnly={true}
        />
        <span id={id} className={styles.checkmark} />
      </label>
    </>
  );
};
