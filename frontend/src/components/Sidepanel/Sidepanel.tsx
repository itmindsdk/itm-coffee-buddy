import { Children, FunctionComponent, useState } from "react";
import styles from './Sidepanel.module.css';

type Props = {
    id: string;
    show: boolean;
}

export const Sidepanel: FunctionComponent<Props> = ({id, show, children}) => {
    return(
        <div 
            id={id}
            className={`${styles.sidepanel} ${!show && styles.hidden}`}
            >
                {children}
        </div>
    )
}