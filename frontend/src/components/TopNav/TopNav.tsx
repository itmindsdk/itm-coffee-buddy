import { FunctionComponent, useContext } from "react";
import ChannelContext from "../../contexts/ChannelContext";
import styles from "./TopNav.module.css";

type Props = {};

export const TopNav: FunctionComponent<Props> = ({}) => {
  const { channels, onChannelSelect } = useContext(ChannelContext);
  return (
    <div className={styles.topNav}>
      <li className={styles.dropdown}>
        <div className={styles.dropbtn}>Channels</div>
        {
          <div className={styles.dropdownContent}>
            { channels.map((channel) => (
              <div
                key={channel.channelId}
                className={styles.dropdownItem}
                id={channel.channelId}
                onClick={onChannelSelect}
              >
                {channel.channelName}
              </div>
            ))} 
          </div>
        }
      </li>
      <p className={styles.title}>Coffee Buddies</p>
    </div>
  );
};
