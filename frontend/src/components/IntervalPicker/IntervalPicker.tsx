import React, {
  FunctionComponent,
  useContext,
} from "react";
import {
  Interval,
  Weekly_Days,
} from "../../../../@types/CoffeeGroups";
import ChannelContext from "../../contexts/ChannelContext";
import { RadioButton } from "../RadioButton/RadioButton";
import styles from "./IntervalPicker.module.css";

type Props = {};

export const IntervalPicker: FunctionComponent<Props> = ({}) => {
  const { updatingIntervalExt, updatingInterval, loading, hasServerResponded, selectedChannel, onIntervalChange, onIntervalExtChange } = useContext(
    ChannelContext
  );

  return (
    <div className={styles.intervalPicker}>
      <b>Coffee Interval</b>
      <div className={styles.intervalContainer}>
        <div className={styles.intervalItem}>
          <RadioButton
            id={Interval.DAILY}
            label="Daily"
            value={selectedChannel.settings.interval == Interval.DAILY}
            disabled={updatingInterval || hasServerResponded}
            onClick={onIntervalChange}
          />
          <RadioButton
            id={Interval.WEEKLY}
            label="Weekly"
            value={selectedChannel.settings.interval == Interval.WEEKLY}
            disabled={updatingInterval || hasServerResponded}
            onClick={onIntervalChange}
          />
          <RadioButton
            id={Interval.BIWEEKLY}
            label="Biweekly"
            value={selectedChannel.settings.interval == Interval.BIWEEKLY}
            disabled={updatingInterval || hasServerResponded}
            onClick={onIntervalChange}
          />
          <RadioButton
            id={Interval.MONTHLY}
            label="Monthly"
            value={selectedChannel.settings.interval == Interval.MONTHLY}
            disabled={updatingInterval || hasServerResponded}
            onClick={onIntervalChange}
          />
        </div>

        {(selectedChannel.settings.interval == Interval.WEEKLY ||
          selectedChannel.settings.interval == Interval.BIWEEKLY) && (
          <div className={styles.intervalItem}>
            <RadioButton
              id={Weekly_Days.MON}
              label="Monday"
              value={selectedChannel.settings.intervalExt == Weekly_Days.MON}
              disabled={updatingInterval || hasServerResponded}
              onClick={onIntervalExtChange}
            />
            <RadioButton
              id={Weekly_Days.TUES}
              label="Tuesday"
              value={selectedChannel.settings.intervalExt == Weekly_Days.TUES}
              disabled={updatingInterval || hasServerResponded}
              onClick={onIntervalExtChange}
            />
            <RadioButton
              id={Weekly_Days.WED}
              label="Wednesday"
              value={selectedChannel.settings.intervalExt == Weekly_Days.WED}
              disabled={updatingInterval || hasServerResponded}
              onClick={onIntervalExtChange}
            />
            <RadioButton
              id={Weekly_Days.THUR}
              label="Thursday"
              value={selectedChannel.settings.intervalExt == Weekly_Days.THUR}
              disabled={updatingInterval || hasServerResponded}
              onClick={onIntervalExtChange}
            />
            <RadioButton
              id={Weekly_Days.FRI}
              label="Friday"
              value={selectedChannel.settings.intervalExt == Weekly_Days.FRI}
              disabled={updatingInterval || hasServerResponded}
              onClick={onIntervalExtChange}
            />
          </div>
        )}
        {selectedChannel.settings.interval == Interval.DAILY && (
          <div className={styles.intervalItem}>
            <form
            >
              <input
                type="number"
                placeholder="Time of day..."
                value={selectedChannel.settings.intervalExt}
                disabled={updatingInterval || hasServerResponded}
                onChange={onIntervalExtChange}
                min="8"
                max="17"
              ></input>
              <input disabled={updatingInterval || hasServerResponded} type="submit" value="Set time of day" />
            </form>
          </div>
        )}
        {selectedChannel.settings.interval == Interval.MONTHLY && (
          <div className={styles.intervalItem}>
            <form
            >
              <input
                type="number"
                placeholder="Day of month..."
                onChange={onIntervalExtChange}
                disabled={updatingInterval || hasServerResponded}
                value={selectedChannel.settings.intervalExt}
                min="1"
                max="31"
              ></input>
              <input disabled={updatingInterval || hasServerResponded} type="submit" value="Set day of month" />
            </form>
          </div>
        )}
      </div>
    </div>
  );
};
