import { FunctionComponent } from "react";
import styles from "./ToggleSwitch.module.css";

type Props = {
    value: boolean,
    onClick: any
}

export const ToggleSwitch: FunctionComponent<Props> = ({value, onClick}) => {
  return (
    <>
      <label  className={styles.switch}>
        <input type="checkbox" className={styles.checkbox} checked={value} onClick={e => onClick(e)}/>
        <span className={styles.slider}></span>
      </label>
    </>
  );
};
