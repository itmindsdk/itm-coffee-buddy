import React, { FunctionComponent, useContext } from "react";
import ChannelContext from "../../contexts/ChannelContext";
import { RadioButton } from "../RadioButton/RadioButton";
import styles from "./MessageToggle.module.css";

type Props = {};

export const MessageToggle: FunctionComponent<Props> = ({}) => {
  const { toggelingGroupMsg, hasServerResponded, selectedChannel, onToggleGroupMessage } = useContext(ChannelContext);

  return (
    <div className={styles.messageToggle}>
      <div className={styles.toggleText}>Enable group messages:</div>
      <RadioButton 
        value={selectedChannel.settings.individualGroupMessagesEnabled}
        disabled={toggelingGroupMsg || hasServerResponded}
        onClick={(e) => onToggleGroupMessage(e)}
      />
    </div>
  );
};
