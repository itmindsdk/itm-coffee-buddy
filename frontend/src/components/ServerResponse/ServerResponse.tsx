import { useContext, useEffect } from 'react';
import ChannelContext from '../../contexts/ChannelContext';
import styles from './ServerResponse.module.css'


const ServerResponse = () => {

    const {error, hasServerResponded, setHasServerResponded, showResponse, serverResponse, setShowResponse } = useContext(ChannelContext);

    useEffect(() => {
        if(hasServerResponded){
            setShowResponse(true);
            setTimeout(() => { setShowResponse(false); }, 1500);
            setHasServerResponded(false);
        }   
    }, [hasServerResponded])

    return (
        <div className={`${showResponse && error? styles.snackbarshow : styles.snackbar} ${!error ? styles.showSucces : styles.showError}`}>
            {serverResponse}
        </div>
    )
}

export default ServerResponse;