import { FunctionComponent } from "react";
import styles from './VariableItem.module.css';

type Props = {
    id?: string;
    code?: string;
    description?: string;
    currentValue?: string;
    onClick?: (e)=> void
}

export const VariableItem: FunctionComponent<Props> = ({id, code, description, currentValue, onClick}) => {
    return(
        <div id={id} className={styles.variableItem} onClick={(e) => {onClick({target: {value: code}})}}>
            {code && <div className={styles.code}>{code}</div>}
            {description && <div className={styles.description}>{description}</div>}
            {currentValue && <div className={styles.currentValue}>{currentValue}</div>}
        </div>
    )
}