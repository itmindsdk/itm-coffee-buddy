import React, {
  FunctionComponent,
  useCallback,
  useContext,
  useEffect,
  useState
} from "react";
import { MessageVariables, msgVarDescription } from "../../../../MessageVariables";
import ChannelContext from "../../contexts/ChannelContext";
import { Sidepanel } from "../Sidepanel/Sidepanel";
import { VariableItem } from "../VariableItem/VariableItem";
import styles from "./MessageConfig.module.css";

type Props = {};

export const MessageConfig: FunctionComponent<Props> = ({}) => {
  const {
    updatingMessage,
    hasServerResponded,
    selectedChannel,
    onMessageSubmit
  } = useContext(ChannelContext);

  const [settings, setSettings] = useState(
    Object.keys(selectedChannel.settings).filter(setting =>
      setting.includes("Template")
    )
  );

  const [settingValues, setSettingValues] = useState(
    Object.values(selectedChannel.settings).filter(
      setting => setting.toString().length > 5
    )
  );

  const [selectedMessage, setSelectedMessage] = useState(String);
  const [selectedTemplate, setSelectedTemplate] = useState(String);

  const changeMessageTemplate = useCallback(
    e => {
      setSelectedMessage(String(settingValues[e.target.value]));
      setSelectedTemplate(String(settings[e.target.value]));
    },
    [selectedChannel, settings, settingValues]
  );

  useEffect(() => {
    setSettings(
      Object.keys(selectedChannel.settings).filter(setting =>
        setting.includes("Template")
      )
    );

    setSettingValues(
      Object.values(selectedChannel.settings).filter(
        setting => setting.toString().length > 5
      )
    ); //why?
  }, [selectedChannel, setSettings, setSettingValues]);

  useEffect(() => {
    setSelectedMessage(String(settingValues[0]));
    setSelectedTemplate(String(settings[0]));
  }, [selectedChannel, settings, settingValues]);

  const onUndo = useCallback(
    e => {
      setSelectedMessage(selectedChannel.settings[selectedTemplate]);
    },
    [selectedMessage, selectedTemplate]
  );

  const insertVariable = e => {
    const textarea = document.querySelector("textarea");
    console.log(e.target.value);
    textarea.setRangeText(
      e.target.value,
      textarea.selectionStart,
      textarea.selectionEnd,
      "end"
    );
    setSelectedMessage(textarea.value);
  };

  const [showSidepanel, setShowSidepanel] = useState(false);

  return (
    <div className={styles.messageConfigContainer}>
      <div className={styles.topnav}>
        <button
          className={styles.burger}
          onClick={() => setShowSidepanel(!showSidepanel)}
        >
          ☰
        </button>
        <select
          defaultValue={selectedTemplate}
          onChange={e => changeMessageTemplate(e)}
        >
          {settings.map(key => (
            <option key={key} value={settings.indexOf(key)}>
              {key.substring(0, key.length - 8)}
            </option>
          ))}
        </select>
        <button
          className={styles.undoButton}
          disabled={updatingMessage || hasServerResponded}
          onClick={e => onUndo(e)}
        >
          Undo
        </button>
        <button
          id={selectedTemplate}
          className={styles.submitButton}
          disabled={updatingMessage || hasServerResponded}
          onClick={e => {
            onMessageSubmit({
              target: { id: selectedTemplate, value: selectedMessage }
            });
          }}
        >
          Submit
        </button>
      </div>

      <div style={{ display: "flex", overflow: "hidden", background: "none", borderBottomLeftRadius: "8px"}}>
        <Sidepanel id="msg-config-sidepanel" show={showSidepanel}>
          {Object.keys(MessageVariables).map(key => {
            return (
              <VariableItem
                key={key}
                id={key}
                code={MessageVariables[key]}
                description={msgVarDescription(MessageVariables[key])}
                onClick={e => insertVariable(e)}
              />
            );
          })}
        </Sidepanel>

        <form
          id={selectedTemplate}
          onSubmit={e => onMessageSubmit(e)}
          style={{ width: "100%" }}
        >
          <textarea
            id="msg-text-area"
            onChange={e => setSelectedMessage(e.target.value)}
            className={styles.msgTextArea}
            rows={13}
            placeholder={selectedMessage}
            value={selectedMessage}
          />
        </form>
      </div>
    </div>
  );
};
