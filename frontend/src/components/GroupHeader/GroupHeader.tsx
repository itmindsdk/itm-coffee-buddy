import { FunctionComponent, useContext } from "react";
import ChannelContext from "../../contexts/ChannelContext";
import styles from "./GroupHeader.module.css";

type Props = {};

export const GroupHeader: FunctionComponent<Props> = ({}) => {
  const { selectedChannel } = useContext(ChannelContext);
  return (
    <div className={styles.channelHeader}>
      <div className={styles.channelName}>{selectedChannel.channelName}</div>
      <div className={styles.channelId}>{selectedChannel.channelId}</div>
    </div>
  );
};
