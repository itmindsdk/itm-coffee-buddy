import { NextPage } from "next";
import Head from "next/head";
import React, { useCallback, useState } from "react";
import { ContentComponent } from "../components/ContentComponent/ContentComponent";
import ChannelContext, { useChannelContext } from "../contexts/ChannelContext";
import styles from "../styles/Home.module.css";

const Index: NextPage = () => {
  const contextValue = useChannelContext();
  const { channels } = contextValue;
  const [focusedChannel, setFocusedChannel] = useState(null);

  const onChannelSelect = useCallback(
    e => {
      console.log(e.target);
      setFocusedChannel(channels.find(ch => ch.channelId === e.target.id));
    },
    [channels, setFocusedChannel, focusedChannel]
  );

  return (
    <>
      <Head>
        <title>Coffee Config</title>
        <link rel="icon" href="./local_cafe-24px.svg" />
        <link
          href="https://fonts.googleapis.com/css2?family=Inconsolata:wght@600&family=Merriweather:ital,wght@1,300&family=Pacifico&display=swap"
          rel="stylesheet"
        ></link>
      </Head>
      <div className={styles.pageContainer}>
        <ChannelContext.Provider value={contextValue}>
          <ContentComponent />
        </ChannelContext.Provider>
      </div>
    </>
  );
};

export default Index;
