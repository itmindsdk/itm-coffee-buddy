export enum MessageVariables {
    NUMBER_OF_GROUPS = "{{1}}",
    NUMBER_GROUPS_DONE = "{{2}}",
    CURRENT_DEADLINE = "{{3}}",
    CURRENT_REMINDER_DATE = "{{4}}"
}

export const msgVarDescription = (msgVar: MessageVariables): string => {
    switch (msgVar) {
        case MessageVariables.NUMBER_OF_GROUPS:
            return "Number of Groups";
        case MessageVariables.NUMBER_GROUPS_DONE:
            return "Number of Groups Done";
        case MessageVariables.CURRENT_DEADLINE:
            return "Current Deadline";
        case MessageVariables.CURRENT_REMINDER_DATE:
            return "Current Reminder Date";
    }
}