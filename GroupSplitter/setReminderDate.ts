export const setReminderDate = (deadline: number) => {
    const today = Date.now();
    return Math.floor((deadline-today)/2 + today);
}