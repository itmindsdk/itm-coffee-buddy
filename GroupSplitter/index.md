Process flow

```plantuml
@startuml
(*) --> "SlackApi: Get all conversations"
--> CHANNEL_FILTER
--> CHANNEL_LOOP
if "Channel exists" then
  -right->[false] (*)
else
  -->[true] CHANNEL_DB_GET
  if "CHECK_CHANNEL_EXISTS" then
    -right-> CHANNEL_DB_CREATE
    --> CHANNEL_DEADLINE
  else
    --> CHANNEL_DEADLINE
  endif

  CHANNEL_DEADLINE --> if "DEADLINE_PAST_DUE" then
    -left->[yes] CHANNEL_LOOP
  else
    -->[no] GROUP_BREAKDOWN

  GROUP_BREAKDOWN --> if "INDIVIDUAL_MSG_ENABLED" then
    -right->[yes] GROUP_NEXT

    if "HAS_NEXT_GROUP" then
      -right-> POST_MSG_GROUP
      -->  GROUP_NEXT
    else
      --> POST_MSG_CONVO
    endif
  else
    -->[no] POST_MSG_CONVO
  endif

  POST_MSG_CONVO -left-> CHANNEL_LOOP
endif

@enduml
```
