import { ChannelSettings, Interval, IntervalExt, Weekly_Days } from "../@types/CoffeeGroups";

const dateValue = (weekDay: Weekly_Days) => {
    switch (weekDay) {
        case Weekly_Days.MON: return 1;
        case Weekly_Days.TUES: return 2;
        case Weekly_Days.WED: return 3;
        case Weekly_Days.THUR: return 4;
        case Weekly_Days.FRI: return 5;
        case Weekly_Days.SAT: return 6;
        case Weekly_Days.SUN: return 0;

    }
}

function isNumber(x: IntervalExt<Interval>): asserts x is number { };

function isString(x: IntervalExt<Interval>): asserts x is Weekly_Days { };

export const setNewDeadline = (channelSettings: ChannelSettings<Interval>) => {
    const now = new Date();
    const day_in_milli = 8.64e7;
    const week_in_milli = 6.048e8;
    const current_day = now.getDay();

    switch (channelSettings.interval) {
        case Interval.DAILY:
            now.setDate(now.getDate() + 1);
            isNumber(channelSettings.intervalExt)
            now.setHours(channelSettings.intervalExt, 0, 0);
            return now.getTime();

        case Interval.WEEKLY:
            var deadline = now.getTime();
            var new_day = 1;

            isString(channelSettings.intervalExt)
            new_day = dateValue(channelSettings.intervalExt);


            // Ensure days are set such that deadlines are at least 1 week in the future.
            if (new_day >= current_day) {
                deadline += (new_day - current_day) * day_in_milli;
            } else {
                deadline += (new_day - current_day + 7) * day_in_milli;
            }

            return deadline + week_in_milli;

        case Interval.BIWEEKLY:
            var deadline = now.getTime();
            var new_day = 1;

            isString(channelSettings.intervalExt)
            new_day = dateValue(channelSettings.intervalExt);

            // Ensure days are set such that deadlines are at least 2 weeks in the future.
            if (new_day >= current_day) {
                deadline += (new_day - current_day) * day_in_milli;
            } else {
                deadline += (new_day - current_day + 7) * day_in_milli;
            }

            return deadline + 2 * week_in_milli;

        case Interval.MONTHLY:
            var month = now.getUTCMonth();
            isNumber(channelSettings.intervalExt)
            now.setMonth(month + 1, channelSettings.intervalExt)
            return now.getTime();
    }
}