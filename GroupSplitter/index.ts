import { AzureFunction, HttpRequest } from "@azure/functions";
import "ts-array-ext";

import { WebClient } from "@slack/web-api";
import { numberToEmoji } from "../getNumberEmoji";
import { Conversation, InteractionType } from "../@types/Slack";
import {
  Channel,
  Group,
  Interval,
  numberOfGroupsHasMet,
  Weekly_Days
} from "../@types/CoffeeGroups";
import { DocTypes } from "../@types/Cosmos";
import { CosmosDB } from "../_cosmos";
import { setNewDeadline } from "./setNewDeadline";

import { archiveGroups } from "./archiveGroups";
import { setReminderDate } from "./setReminderDate";
import { getChannelBotIsMemberOf } from "../_slack/getChannelBotIsMemberOf";

function isNumber(x: any): asserts x is number {}

const httpTrigger: AzureFunction = async (context, req: HttpRequest) => {
  context.log("HTTP trigger function processed a request.");
  context.res = {
    status: 200
  };

  const token = process.env.SLACK_TOKEN;
  const web = new WebClient(token);

  const ownBotId = await web.auth.test().then(res => res.user_id);

  const allConversations = await getChannelBotIsMemberOf();

  const container = await new CosmosDB().initialize();

  await Promise.all(
    allConversations.map(async convo => {
      const now = Date.now();
      const channel: Channel<Interval> = new Channel(
        await container
          .retype(DocTypes.CoffeeGroups)
          .where({
            key: "channelId",
            val: convo.id
          })
          .firstOrDefault(
            {
              channelId: convo.id,
              //currentdeadline is now-1 to ensure that new channels are shuffled and grouped immediately.
              channelName: convo.name,
              currentDeadline: now - 1,
              reminderDate: now,
              currentGroups: [],
              memberIdsOnPause: [],
              settings: {
                groupSize: 3,
                individualGroupMessagesEnabled: false,
                individualGroupMessageTemplate: `Hej.\nI skal ud og drikke kaffe sammen! Hurrah!\nI kan bruge denne chat til at planlægge jeres kaffetur.\nJeg vil spørge Jer hver fredag om I er blevet færdige.\nHav en god tur <3`,
                channelMessageTemplate:
                  "TIME FOR COFFEE! <!channel>\nOver de næste to uger skal alle grupper ud og tage en dejlig kop is kaffe :glass_of_milk: :coffee: og også gerne en selfie. Hav en god tur :smile:\nGrupperne:\n",
                selfieUploadedTemplate:
                  "I have nu taget et selfie, og det er registeret at i har drukket kaffe",
                channelReportTemplate:
                  "{{2}} ud af {{1}} grupper har drukket kaffe :glass_of_milk: :coffee:.",
                coffeeDoneTemplate: "Nice! :glass_of_milk: :coffee:",
                reminderMessageTemplate:
                  "Husk at drikke jeres kaffe før: {{1}} :glass_of_milk: :coffee:\n",
                selfieReminderTemplate:
                  "Husk at uploade en selfie. :glass_of_milk: :coffee:",
                resumeCoffeeTemplate:
                  "Du er nu med igen fra og med næste runde",
                resumeErrorMessageTemplate:
                  "Du er allerede med. Der er ingen grund til at gøre dette igen",
                onPauseMessageTemplate:
                  "Du er på kaffepause :glass_of_milk: :coffee:.",
                alreadyOnPauseMessageTemplate:
                  "Du er allerede på kaffepause :glass_of_milk: :coffee:.",
                groupClosedTemplate:
                  "Denne gruppe er ikke længere aktiv. :coffee:",
                interval: Interval.BIWEEKLY,
                intervalExt: Weekly_Days.MON
              },
              type: DocTypes.CoffeeGroups
            },
            { saveDefault: true }
          )
      );

      if (channel.currentDeadline > now) return;

      if (channel.currentDeadline !== now - 1) archiveGroups(channel);

      // If it is a newly created channel don't send out coffee drinking statistics.
      if (channel.currentGroups.length > 0) {
        await web.chat.postMessage({
          text: channel.channelReportMessage,
          channel: channel.channelId
        });
      }

      const members = await web.conversations
        .members({
          channel: convo.id
        })
        .then(res => res.members as string[])
        .then(members =>
          members.filter(
            mem => mem !== ownBotId && !channel.memberIdsOnPause.includes(mem)
          )
        );

      channel.currentDeadline = setNewDeadline(channel.settings);
      channel.reminderDate = setReminderDate(channel.currentDeadline);
      channel.currentGroups = members
        .shuffle()
        .chunkBySize(channel.settings.groupSize, true, true)
        .map<Group>(memberIds => ({
          groupCreatedTs: null,
          hasMet: false,
          isReminded: false,
          memberIds
        }));

      const groupBreakdown = channel.currentGroups
        .map(
          (group: { memberIds: string[] }, index: number) =>
            `${numberToEmoji(index + 1)}\n>` +
            group.memberIds.map((u: string) => `<@${u}>`).join(" ")
        )
        .join("\n");

      await web.chat.postMessage({
        text: channel.channelMessage + "\n" + groupBreakdown,
        channel: convo.id
      });

      if (channel.settings.individualGroupMessagesEnabled) {
        const groupMessages = channel.currentGroups.map(async group => {
          if (group.memberIds.length === 1) {
            //This case shouldn't exist but we keep it should there be a business logic change.
            group.channelId = group.memberIds[0];
          } else {
            group.channelId = await web.conversations
              .open({
                users: [...group.memberIds, ownBotId].join(",")
              })
              .then(res => (res.channel as Conversation).id);
          }

          await web.chat
            .postMessage({
              text: channel.individualGroupMessage,
              channel: group.channelId,
              blocks: [
                {
                  type: "section",
                  text: {
                    type: "mrkdwn",
                    text: channel.individualGroupMessage
                  }
                },
                {
                  type: "actions",
                  elements: [
                    {
                      type: "button",
                      text: {
                        type: "plain_text",
                        text: "Done"
                      },
                      style: "primary",
                      value: group.channelId,
                      action_id: InteractionType.COFFEE_GROUP_HAS_MET
                    }
                  ]
                }
              ]
            })
            .then(msgResponse => {
              isNumber(msgResponse.ts);
              group.groupCreatedTs = msgResponse.ts.toString();
            });
        });

        await Promise.all(groupMessages);
      }

      await container.upsertInDB(channel);
    })
  );
};

export default httpTrigger;
