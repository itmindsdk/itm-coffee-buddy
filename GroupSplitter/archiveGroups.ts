import { Channel, Interval } from "../@types/CoffeeGroups";
import { DocTypes } from "../@types/Cosmos";
import { CosmosDB } from "../_cosmos";


export const archiveGroups = async (Channel: Channel<Interval>) => {
    const archives = await new CosmosDB().initialize();
    const archive = await archives
        .retype(DocTypes.GroupArchives)
        .where({
            key: "deadline",
            val: Channel.currentDeadline
        })
        .firstOrDefault(
            {
                channelId: Channel.channelId,
                deadline: Channel.currentDeadline,
                groups: Channel.currentGroups,
                type: DocTypes.GroupArchives
            },
            { saveDefault: true }
        );

    await archives.upsertInDB(archive);

}