import { WebClient } from "@slack/web-api";
import { Conversation } from "../@types/Slack";

export const getChannelBotIsMemberOf = async () => {
  const token = process.env.SLACK_TOKEN;
  const web = new WebClient(token);

  const ownBotId = await web.auth.test().then(res => res.user_id as string);

  const allConversations = await web.users
    .conversations({
      types: "public_channel,private_channel,im",
      exclude_archived: true,
      limit: 1000,
      user: ownBotId
    })
    .then(res => res.channels as Conversation[]);

  return allConversations;
};
