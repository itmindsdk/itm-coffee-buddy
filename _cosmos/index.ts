import {
  CosmosClient,
  Container,
  SqlQuerySpec,
  SqlParameter
} from "@azure/cosmos";
import { Base, DocTypes, typeMap } from "../@types/Cosmos";

type KeyValPair<T extends Base = Base, K extends keyof T = "id"> = {
  key: K;
  val: T[K];
};

type DefaultQuerySettings = {
  saveDefault: boolean;
};

/**
 *Usage:
 * ```typescript
 * const container = await new CosmosDB().initialize();
 * container.upsertInDB(
 *  // any obj
 * );
 * ```
 */
export class CosmosDB<T extends Base> {
  private container: Container;
  private initializedVal: boolean = false;
  private filterConditions: ((x: Base) => boolean)[] = [];
  private sqlConditions: KeyValPair<any, any>[] = [];
  private typeRef?: DocTypes;

  get initialized() {
    return this.initializedVal;
  }

  async initialize() {
    this.container = await ensureSetup();
    this.initializedVal = true;
    return this;
  }

  retype(type: DocTypes) {
    const newC = typeMap(type);
    newC.container = this.container;
    newC.initializedVal = true;
    // newC.sqlConditions = this.sqlConditions;
    // newC.filterConditions = this.filterConditions;
    newC.typeRef = type;
    return newC;
  }

  private checkInitilized() {
    if (!this.initializedVal) {
      throw Error(
        "CosmosDB not initilizes. Try `await new CosmosDB().initialize()`"
      );
    }
  }

  private conditionsToQuery() {
    const queryPart =
      this.sqlConditions.length > 0
        ? " WHERE " +
          this.sqlConditions
            .map(con => `(c.${con.key} = @${con.key})`)
            .join(" AND ")
        : "";

    const paramPart = this.sqlConditions.map(
      con =>
        <SqlParameter>{
          name: `@${con.key}`,
          value: con.val
        }
    );

    const query: SqlQuerySpec = {
      query: "SELECT * FROM c" + queryPart,
      parameters: paramPart
    };
    return query;
  }

  private execute() {
    return this.container.items
      .query<T>(this.conditionsToQuery(), {
        partitionKey: process.env.COSMOS_VERSION
      })
      .fetchAll()
      .then(res =>
        res.resources.filter(r => this.filterConditions.every(f => f(r)))
      );
  }

  where<V extends T, K extends keyof V>(
    fn: KeyValPair<V, K> | ((x: V) => boolean)
  ) {
    const newC = this.retype(this.typeRef);
    if (typeof fn === "function") {
      newC.filterConditions.push(fn);
    } else {
      newC.sqlConditions.push(fn);
    }

    return newC;
  }

  upsertInDB(item: T) {
    this.checkInitilized();

    item._version = process.env.COSMOS_VERSION;

    return this.container.items.upsert<T>(item).then(res => res.resource);
  }

  deleteFromDb(item: T){
      this.checkInitilized();
      item._version = process.env.COSMOS_VERSION;
      return this.container.item(item.id, item._version).delete<T>().then(res => res.resource);
  }

  first() {
    return this.firstOrDefault(undefined);
  }

  firstOrDefault(defaultVal: T = null, settings?: DefaultQuerySettings) {
    return this.execute().then(res => {
      if (res[0]) {
        return res[0];
      }
      if (settings?.saveDefault) {
        return this.upsertInDB(defaultVal);
      }
      return defaultVal;
    });
  }

  readAllMatchedEntries() {
    return this.execute().then(res => {
      if (res[0]) {
        return res;
      }

      return [];
    });
  }
}

const ensureSetup = async () => {
  const key = process.env.COSMOS_KEY;
  const endpoint = process.env.COSMOS_URL;
  const containerId = process.env.COSMOS_CONTAINER;
  const databaseId = process.env.COSMOS_DBID;
  const partitionKey = {
    kind: "hash",
    paths: ["/_version"]
  };

  const client = new CosmosClient({ endpoint, key });

  const { database } = await client.databases.createIfNotExists({
    id: databaseId
  });

  const { container } = await client
    .database(database.id)
    .containers.createIfNotExists({ id: containerId, partitionKey });

  return container;
};
