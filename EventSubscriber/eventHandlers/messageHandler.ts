import { Context } from "@azure/functions";
import { WebClient } from "@slack/web-api";
import { DocTypes } from "../../@types/Cosmos";
import { Channel, Group, Interval } from "../../@types/CoffeeGroups";
import {
  Conversation,
  EventSubType,
  FilesInfo,
  MessageEvent
} from "../../@types/Slack";
import { CosmosDB } from "../../_cosmos";
import { getChannelBotIsMemberOf } from "../../_slack/getChannelBotIsMemberOf";

export const messageHandler = async (context: Context, msg: MessageEvent) => {
  context.log(msg.subtype);
  const token = process.env.SLACK_TOKEN;
  const web = new WebClient(token);

  switch (msg?.subtype) {
    case EventSubType.FILE_SHARE:
      const file = msg.files.map(file => file.id);
      var info: FilesInfo;

      const allConversations = await getChannelBotIsMemberOf();

      const container = await new CosmosDB().initialize();

      await fetch(
        `https://slack.com/api/files.info?token=${token}&file=${file}`
      )
        .then(response => response.json())
        .then(json => (info = json as FilesInfo))
        .catch(error => context.log(`Error: ${error}`));

      await Promise.all(
        allConversations.map(async convo => {
          const channel: Channel<Interval> = new Channel(
            await container
              .retype(DocTypes.CoffeeGroups)
              .where({
                key: "channelId",
                val: convo.id
              })
              .first()
          );

          var channelId: string;
          var group: Group;
          if (info.file.channels) {
            channelId = info.file.channels[0];
            group = channel.currentGroups.find(group =>
              group.memberIds.includes(msg.user)
            );
            if (!group) return;
          } else if (info.file.groups) {
            channelId = info.file.groups[0];
            group = channel.currentGroups.find(
              group => group.channelId === channelId
            );
            if (!group) return;
          } else if (info.file.ims) {
            channelId = info.file.ims[0];
          }

          group.hasMet = true;
          group.photoUrl = msg.files[0].id;
          await container.upsertInDB(channel);

          if (group.groupCreatedTs) {
            web.chat.update({
              channel: group.channelId,
              ts: group.groupCreatedTs,
              token: token,
              text:
                channel.individualGroupMessage +
                "\n" +
                channel.selfieUploadedMessage,
              blocks: [
                {
                  type: "section",
                  text: {
                    type: "mrkdwn",
                    text:
                      channel.individualGroupMessage +
                      "\n" +
                      channel.selfieUploadedMessage
                  }
                }
              ]
            });
          } else {
            web.chat.postEphemeral({
              user: msg.user,
              text: channel.selfieUploadedMessage,
              channel: msg.channel
            });
          }
        })
      );
      break;
    case EventSubType.RENAME_CHANNEL:
    case EventSubType.RENAME_PRIVATE_CHANNEL: {
      const convo = allConversations.find(convo => convo.id === msg.channel);
      const conversation = new Channel(
        await container
          .retype(DocTypes.CoffeeGroups)
          .where({
            key: "channelId",
            val: convo.id
          })
          .first()
      );
      conversation.channelName = msg.name;
      await container.upsertInDB(conversation);
      break;
    }
    default: {
      const isRemovedEvent = RegExp(/^You have been removed/).test(msg.text);
      const isSlackBot = msg.user === "USLACKBOT";

      const isPrivateChannel = RegExp(/private channel/).test(msg.text);
      const isPublicChannel = RegExp(/#/).test(msg.text);
      var channelName: string;

      if (isRemovedEvent && isSlackBot) {
        if (isPrivateChannel) {
          channelName = msg.text
            .split("You have been removed from the private channel")[1]
            .split("by")[0]
            .trim();
        } else if (isPublicChannel) {
          channelName = msg.text.split("#")[1].split("by")[0].trim();
        }

        const channel = new Channel(
          await container
            .retype(DocTypes.CoffeeGroups)
            .where({
              key: "channelName",
              val: channelName
            })
            .first()
        );

        if (!channel) {
          break;
        }

        const archive = await container
          .retype(DocTypes.GroupArchives)
          .where({
            key: "deadline",
            val: channel.currentDeadline
          })
          .firstOrDefault(
            {
              channelId: channel.channelId,
              deadline: channel.currentDeadline,
              groups: channel.currentGroups,
              type: DocTypes.GroupArchives
            },
            { saveDefault: true }
          );

        await container.upsertInDB(archive);
        await container.deleteFromDb(channel);
      }
      break;
    }
  }
};
