import { AzureFunction, HttpRequest } from "@azure/functions";
import { fileHandler } from "./eventHandlers/fileHandler";
import { messageHandler } from "./eventHandlers/messageHandler";
import { Message, EventType, FileEvent, MessageEvent } from "../@types/Slack";

const httpTrigger: AzureFunction = async (context, req: HttpRequest) => {
  const challenge = req.body?.challenge ?? req.query?.challenge ?? "No_Challenge";
  context.res = {
    status: 200,
    body: challenge
  };

  const message = req.body as Message;
  console.log(req);

  switch (message?.event?.type) {
    case EventType.FILE_CREATED:
      fileHandler(message.event as FileEvent);
      break;
    case EventType.MESSAGE:
      await messageHandler(context, message.event as MessageEvent);
      break;
    default:
      context.res = {
        status: 400
      };
      return;
  }
};

export default httpTrigger;
