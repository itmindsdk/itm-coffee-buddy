import { AzureFunction, Context, HttpRequest } from "@azure/functions"
import { WebClient } from "@slack/web-api";
import { Channel, Interval, StatusVM } from "../@types/CoffeeGroups";
import { DocTypes } from "../@types/Cosmos";
import { GroupStatus, Members } from "../@types/Slack";
import { CosmosDB } from "../_cosmos";
import { getChannelBotIsMemberOf } from "../_slack/getChannelBotIsMemberOf";

const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
    context.log('HTTP trigger function processed a request.');

    let memberListResponse: Members
    let statusVM: StatusVM[] = [];

    const token = process.env.SLACK_TOKEN;
    const web = new WebClient(token);

    await web.users.list({
        token: token
    }).then((response: unknown) => {
        memberListResponse = response as Members
    })

    const allConversations = await getChannelBotIsMemberOf();

    const container = await new CosmosDB().initialize();

    await Promise.all(allConversations.map(async convo => {
        await container
            .retype(DocTypes.CoffeeGroups)
            .where({
                key: "channelId",
                val: convo.id
            }).first()
            .then((ch: Channel<Interval>) => {
                if (!ch) return
                let groupStatuses: GroupStatus[] = [];
                ch.currentGroups.map(group => {
                    let memberNames: string[] = [];

                    group.memberIds.map(person => {
                        const index = memberListResponse.members.findIndex(member => member.id === person)
                        memberNames.push(memberListResponse.members[index].profile.real_name_normalized)
                    })

                    const status: GroupStatus = {
                        members: memberNames,
                        hasmet: group.hasMet
                    }
                    groupStatuses.push(status);
                })
                statusVM.push(new StatusVM(ch.channelId, ch.channelName, ch.currentDeadline, groupStatuses));
            })
    }))

    context.res = {
        status: 200,
        body: statusVM
    };
};

export default httpTrigger;