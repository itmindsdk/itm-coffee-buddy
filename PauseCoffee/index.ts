import { AzureFunction, HttpRequest } from "@azure/functions";
import "ts-array-ext";

import { WebClient } from "@slack/web-api";
import { Conversation } from "../@types/Slack";
import { CosmosDB } from "../_cosmos";
import { DocTypes } from "../@types/Cosmos";
import { Channel, Interval } from "../@types/CoffeeGroups";
import { getChannelBotIsMemberOf } from "../_slack/getChannelBotIsMemberOf";

const httpTrigger: AzureFunction = async (context, req: HttpRequest) => {
  context.log("HTTP trigger function PauseCoffee processed a request.");
  // Send 200 back immediately such that slack doesn't timeout.
  context.res = {
    status: 200
  };

  let coffeeChannel: Channel<Interval>;
  let memberOnPause = false;

  const params = new URL(req.url + "?" + req.body).searchParams;

  const token = process.env.SLACK_TOKEN;
  const web = new WebClient(token);

  const allConversations = await getChannelBotIsMemberOf();

  const container = await new CosmosDB().initialize();

  await Promise.all(
    allConversations.map(async convo => {
      await container
        .retype(DocTypes.CoffeeGroups)
        .where({
          key: "channelId",
          val: convo.id
        })
        .first()
        .then((channel: Channel<Interval>) => {
          switch (params.get("channel_name")) {
            case "privategroup":
              if (
                !channel?.currentGroups.find(
                  group => group.channelId === params.get("channel_id")
                )
              ) {
                return;
              }

              coffeeChannel = new Channel(channel);
              memberOnPause = channel?.memberIdsOnPause.includes(
                params.get("user_id")
              );
              break;
            default:
              if (channel?.channelId === params.get("channel_id")) {
                coffeeChannel = new Channel(channel);
                memberOnPause = channel?.memberIdsOnPause.includes(
                  params.get("user_id")
                );
              }
              break;
          }
        });
      if (coffeeChannel) return;
    })
  );

  if (!memberOnPause) {
    coffeeChannel.memberIdsOnPause.push(params.get("user_id"));
    container.upsertInDB(coffeeChannel);
  }

  web.chat.postEphemeral({
    user: params.get("user_id"),
    text: memberOnPause
      ? coffeeChannel.alreadyOnPauseMessage
      : coffeeChannel.onPauseMessage,
    channel: params.get("channel_id")
  });

  context.res = {
    status: 200
  };
};

export default httpTrigger;
