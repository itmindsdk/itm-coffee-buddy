import { AzureFunction, Context } from "@azure/functions";
import { WebClient } from "@slack/web-api";
import { Channel, Interval } from "../@types/CoffeeGroups";
import { DocTypes } from "../@types/Cosmos";
import { Conversation } from "../@types/Slack";
import { CosmosDB } from "../_cosmos";
import { getChannelBotIsMemberOf } from "../_slack/getChannelBotIsMemberOf";

const timerTrigger: AzureFunction = async function (
  context: Context,
  groupReminder: any
): Promise<void> {
  const token = process.env.SLACK_TOKEN;
  const web = new WebClient(token);
  const now = Date.now();

  const allConversations = await getChannelBotIsMemberOf();

  const container = await new CosmosDB().initialize();

  allConversations.forEach(async convo => {
    const channel: Channel<Interval> = new Channel(
      await container
        .retype(DocTypes.CoffeeGroups)
        .where({
          key: "channelId",
          val: convo.id
        })
        .first()
    );
    await Promise.all(
      channel.currentGroups
        .filter(
          group =>
            !group.hasMet && !group.isReminded && now >= channel.reminderDate
        )
        .map(group => {
          web.chat.postMessage({
            text: channel.reminderMessage,
            channel: group.channelId
          });
          group.isReminded = true;
          container.upsertInDB(channel);
        })
    );
  });
};

export default timerTrigger;
