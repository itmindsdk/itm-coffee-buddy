import { AzureFunction, Context, HttpRequest } from "@azure/functions";
import { WebClient } from "@slack/web-api";
import {
  Channel,
  Interval,
  numberOfGroupsHasMet
} from "../@types/CoffeeGroups";
import { DocTypes } from "../@types/Cosmos";
import { Conversation } from "../@types/Slack";
import { CosmosDB } from "../_cosmos";
import { getChannelBotIsMemberOf } from "../_slack/getChannelBotIsMemberOf";

const httpTrigger: AzureFunction = async function (
  context: Context,
  req: HttpRequest
): Promise<void> {
  context.log("HTTP trigger function GenerateReport processed a request.");
  context.res = {
    status: 200
  };

  let channel: Channel<Interval>;

  let params = new URL(req.url + "?" + req.body).searchParams;

  const token = process.env.SLACK_TOKEN;
  const web = new WebClient(token);

  const allConversations = await getChannelBotIsMemberOf();

  const container = await new CosmosDB().initialize();

  await Promise.all(
    allConversations.map(async convo => {
      await container
        .retype(DocTypes.CoffeeGroups)
        .where({
          key: "channelId",
          val: convo.id
        })
        .first()
        .then((ch: Channel<Interval>) => {
          switch (params.get("channel_id")) {
            case "privategroup":
              if (
                !ch?.currentGroups.find(
                  group => group.channelId === params.get("channel_id")
                )
              )
                return;

              channel = new Channel(ch);
              break;
            default:
              if (!(ch?.channelId === params.get("channel_id"))) return;

              channel = new Channel(ch);
              break;
          }
        });
      if (channel) return;
    })
  );

  if (!channel) {
    await web.chat.postEphemeral({
      user: params.get("user_id"),
      text: "Error occured when generating report.",
      channel: params.get("channel_id")
    });
  } else {
    console.log(channel.channelReportMessage);
    await web.chat.postMessage({
      text: channel.channelReportMessage,
      channel: channel.channelId
    });
  }
};

export default httpTrigger;
