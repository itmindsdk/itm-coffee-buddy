import { MessageVariables } from "../MessageVariables";
import { Base, DocTypes } from "./Cosmos";
import { GroupStatus } from "./Slack";

export interface Group {
  groupCreatedTs: string;
  channelId?: string;
  hasMet: boolean;
  isReminded: boolean;
  photoUrl?: string;
  memberIds: string[];
}

export enum Interval {
  DAILY,
  WEEKLY,
  BIWEEKLY,
  MONTHLY
}

/// Interval ext.
export enum Weekly_Days {
  MON = "MON",
  TUES = "TUES",
  WED = "WED",
  THUR = "THUR",
  FRI = "FRI",
  SAT = "SAT",
  SUN = "SUN"
}

export type IntervalExt<T extends Interval> = T extends Interval.WEEKLY
  ? Weekly_Days
  : T extends Interval.BIWEEKLY
  ? Weekly_Days
  : number;

export type ChannelSettings<T extends Interval> = {
  individualGroupMessagesEnabled: boolean;
  individualGroupMessageTemplate: string;
  reminderMessageTemplate: string;
  channelMessageTemplate: string;
  selfieUploadedTemplate: string;
  channelReportTemplate: string;
  resumeCoffeeTemplate: string;
  resumeErrorMessageTemplate: string;
  onPauseMessageTemplate: string;
  alreadyOnPauseMessageTemplate: string;
  coffeeDoneTemplate: string;
  selfieReminderTemplate: string;
  groupClosedTemplate: string;
  groupSize: number;
  interval: T;
  intervalExt: IntervalExt<T>;
};

export class Channel<T extends Interval = Interval.BIWEEKLY> implements Base {
  type?: DocTypes;
  id?: string;
  _version?: string;
  channelId: string;
  channelName: string;
  currentDeadline: number;
  reminderDate: number;
  currentGroups: Group[];
  allPhotos: string[];
  memberIdsOnPause: string[];
  settings: ChannelSettings<T>;

  constructor(
    obj: Partial<Channel<T>>
  ) {
    this.type = obj.type;
    this.id = obj.id;
    this._version = obj._version;
    this.channelId = obj.channelId;
    this.channelName = obj.channelName;
    this.currentDeadline = obj.currentDeadline;
    this.reminderDate = obj.reminderDate;
    this.currentGroups = obj.currentGroups;
    this.allPhotos = obj.allPhotos;
    this.memberIdsOnPause = obj.memberIdsOnPause;
    this.settings = obj.settings;
  }

  get individualGroupMessage(): string {
    return this.replaceMessageVariables(this.settings.individualGroupMessageTemplate);
  }
  get reminderMessage(): string {
    return this.replaceMessageVariables(this.settings.reminderMessageTemplate);
  }

  get channelMessage(): string {
    return this.replaceMessageVariables(this.settings.channelMessageTemplate);
  }

  get selfieUploadedMessage(): string {
    return this.replaceMessageVariables(this.settings.selfieUploadedTemplate);
  }

  get channelReportMessage(): string {
    return this.replaceMessageVariables(this.settings.channelReportTemplate);
  }

  get resumeCoffeeMessage(): string {
    return this.replaceMessageVariables(this.settings.resumeCoffeeTemplate);
  }

  get resumeErrorMessage(): string {
    return this.replaceMessageVariables(this.settings.resumeErrorMessageTemplate);
  }

  get onPauseMessage(): string {
    return this.replaceMessageVariables(this.settings.onPauseMessageTemplate);
  }

  get alreadyOnPauseMessage(): string {
    return this.replaceMessageVariables(this.settings.alreadyOnPauseMessageTemplate);
  }
  
  get coffeeDoneMessage(): string {
    return this.replaceMessageVariables(this.settings.coffeeDoneTemplate);
  }

  private replaceMessageVariables(template : string) : string {
    let result = template;
    Object.values(MessageVariables).forEach((value) => {
      const test = template.includes(value);
      if(template.includes(value)) {
        switch(value){
          case MessageVariables.NUMBER_GROUPS_DONE:
            result = result.replace(value, numberOfGroupsHasMet(this).toString())
            break;
          case MessageVariables.NUMBER_OF_GROUPS:
            result = result.replace(value, this.currentGroups.length.toString());
            break;
          case MessageVariables.CURRENT_DEADLINE:
            result = result.replace(value, new Date(this.currentDeadline).toDateString());
            break;
          case MessageVariables.CURRENT_REMINDER_DATE:
            result = result.replace(value, new Date(this.reminderDate).toDateString())
            break;
        }
      }
    });
    return result;
  }
}

export interface ChannelArchive extends Base {
  channelId: string;
  deadline: number;
  groups: Group[];
}

export class ChannelVM<T extends Interval = Interval.BIWEEKLY> {
  constructor(
    channelName: string,
    channelId: string,
    settings: ChannelSettings<T>,

  ) {
    (this.channelName = channelName),
      (this.channelId = channelId),
      (this.settings = settings);
  }

  channelName: string;
  channelId: string;
  settings: ChannelSettings<T>;
}

export class ImageCollageVM {
  constructor(
    channelId: string,
    channelName: string,
    photoUrls: string[],
    error: {
      deadline: number;
      fileId: string;
      error: string;
      groupId: string;
    }[],
  ) {
    (this.channelId = channelId),
    (this.channelName = channelName),
    (this.photoUrls = photoUrls),
    (this.error = error);
  }

  channelId: string;
  channelName: string;
  photoUrls: string[];
  error: {
    deadline: number;
    fileId: string;
    error: string;
    groupId: string;
  }[];
}
export class StatusVM {
  constructor(
    channelId: string,
    channelName: string,
    deadline: number,
    completeStatus: GroupStatus[]
  ) {
    (this.channelId = channelId),
    (this.channelName = channelName),
    (this.deadline = deadline),
    (this.completeStatus = completeStatus)
  }

  channelId: string
  channelName: string;
  deadline: number;
  completeStatus: GroupStatus[];
}

export const numberOfGroupsHasMet = (channel: Channel<Interval>): number => {
  return channel.currentGroups.filter(group => group.hasMet).length;
};
