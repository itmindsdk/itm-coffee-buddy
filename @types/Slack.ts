export enum EventType {
  FILE_CREATED = "file_created",
  MESSAGE = "message",
  USER_LEFT = "member_left_channel"
}

export enum EventSubType {
  FILE_SHARE = "file_share",
  RENAME_CHANNEL = "channel_name",
  RENAME_PRIVATE_CHANNEL = "group_name",
  CHANNEL_LEAVE = "channel_leave",
  PRIVATE_CHANNEL_LEAVE = "group_leave"
}

export interface FileUpload {
  id: string;
  created: number;
  timestamp: number;
  user: String;
}

export interface FilesInfo {
  mimetype: string;
  is_public: boolean;
  file: {
    url_private: string;
    channels: string[];
    groups: string[];
    ims: string[];
  };
}

export enum EventSubType {
  CHANNEL_NAME = "channel_name",
  GROUP_NAME = "group_name"
}

export enum InteractionType {
  COFFEE_GROUP_HAS_MET = "coffee_group_has_met"
}

export enum CommandType {
  COFFEE_DONE = "/coffee-done",
  PAUSE_COFFEE = "/pause-coffee",
  RESUME_COFFEE = "/resume-coffee"
}

export interface Event {
  type: EventType;
  subtype: EventSubType;
}

export interface UnknownEvent extends Event {}

export interface FileEvent extends Event {
  type: EventType.FILE_CREATED;
  file_id: string;
  user_id: string;
  file: { id: string };
  event_ts: string;
}

export interface MessageEvent extends Event {
  type: EventType.MESSAGE;
  subtype: EventSubType;
  files: FileUpload[];
  channel: string;
  user: string;
  text: string;
  ts: number;
  event_ts: number;
  channel_type: "mpim" | "ip" | "channel" | "group";
  name: string;
}

export interface UserLeftEvent extends Event {
  type: EventType.USER_LEFT;
  channel: string;
  user: string;
}

export interface Message<T extends Event = UnknownEvent> {
  token: string;
  team_id: string;
  api_app_id: string;
  event: T;
  type: string;
  event_id: string;
  event_time: number;
  authed_users: string[];
}

export interface Conversation {
  id: string;
  name: string;
  is_channel: boolean;
  is_group: boolean;
  is_im: boolean;
  created: number;
  is_archived: boolean;
  is_general: boolean;
  unlinked: number;
  name_normalized: string;
  is_shared: boolean;
  parent_conversation: unknown;
  creator: string;
  is_ext_shared: boolean;
  is_org_shared: boolean;
  shared_team_ids: string[];
  pending_shared: unknown[];
  pending_connected_team_ids: unknown[];
  is_pending_ext_shared: boolean;
  is_member: boolean;
  is_private: boolean;
  is_mpim: boolean;
  topic: {
    value: string;
    creator: string;
    last_set: number;
  };
  purpose: {
    value: string;
    creator: string;
    last_set: number;
  };
  previous_names: string[];
  num_members: number;
}

export interface Bot {
  id: string;
  deleted: boolean;
  name: string;
  updated: number;
  app_id: string;
  user_id: string;
  icons: {
    image_36: string;
    image_48: string;
    image_72: string;
  };
}

export interface SlashCommand {
  token: string;
  command: string;
  text: string;
  response_url: string;
  trigger_id: string;
  user_id: string;
  user_name: string;
  team_id: string;
  enterprise_id: string;
  channel_id: string;
  api_app_id: string;
}

export interface Interaction {
  response_url: string;
  type: string;
  trigger_id: string;
  user: object;
  channel: object;
  actions: Array<Action>;
  container: {
    type: string;
    message_ts: string;
    channel_id: string;
    is_ephemeral: boolean;
  };
}

export interface Action {
  action_id: InteractionType;
  block_id: string;
  value: string;
  style: string;
  type: string;
  action_ts: string;
}

export interface Members {
  ok: boolean;
  members: Member[];
}

export interface Member {
  id: string;
  profile: {
    real_name_normalized?: string;
  }
}

export interface GroupStatus {
  members: string[];
  hasmet: boolean;
}