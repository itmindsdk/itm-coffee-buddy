import { Channel, ChannelArchive } from "./CoffeeGroups";
import { CosmosDB } from "../_cosmos";

export enum DocTypes {
  CoffeeGroups,
  GroupArchives,
  ExcludedUsers
}

export interface Base {
  type?: DocTypes;
  id?: string;
  _version?: string;
}

export const typeMap = (em: DocTypes): CosmosDB<any> => {
  switch (em) {
    case DocTypes.CoffeeGroups:
      return new CosmosDB<Channel>();
    case DocTypes.GroupArchives:
      return new CosmosDB<ChannelArchive>();
  }
};
