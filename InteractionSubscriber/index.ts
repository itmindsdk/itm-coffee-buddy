import { AzureFunction, Context, HttpRequest } from "@azure/functions"
import { Interaction, InteractionType } from "../@types/Slack";
import { hasMetHandler } from "./interactionHandlers/hasMetHandler";

const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
    context.log('HTTP trigger InteractionSubscriber function processed a request.');
    // Send 200 back immediately such that slack doesn't timeout.
    context.res = {
        status: 200,
    };

    const message = JSON.parse(decodeURIComponent(req.body).replace("payload=", "")) as Interaction;

    message.actions.map(action => {
        switch (action.action_id) {
            case InteractionType.COFFEE_GROUP_HAS_MET:
                hasMetHandler(action, message.container.message_ts);
        }
    });
};

export default httpTrigger;