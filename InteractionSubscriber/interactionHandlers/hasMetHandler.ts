import { WebClient } from "@slack/web-api";
import { Channel, Interval } from "../../@types/CoffeeGroups";
import { DocTypes } from "../../@types/Cosmos";
import { Action, Conversation } from "../../@types/Slack";
import { CosmosDB } from "../../_cosmos";
import { getChannelBotIsMemberOf } from "../../_slack/getChannelBotIsMemberOf";

export const hasMetHandler = async (
  action: Action,
  messageTimestamp: string
) => {
  const slackToken = process.env.SLACK_TOKEN;
  const web = new WebClient(slackToken);

  const allConversations = await getChannelBotIsMemberOf();

  const container = await new CosmosDB().initialize();

  allConversations.forEach(async convo => {
    const channel: Channel<Interval> = new Channel(
      await container
        .retype(DocTypes.CoffeeGroups)
        .where({
          key: "channelId",
          val: convo.id
        })
        .first()
    );

    const group = channel.currentGroups.find(
      group => group.channelId === action.value
    );

    if (!group) return;

    group.hasMet = true;
    await container.upsertInDB(channel);
    await web.chat.update({
      channel: action.value,
      ts: messageTimestamp,
      token: slackToken,
      text: channel.individualGroupMessage,
      blocks: [
        {
          type: "section",
          text: {
            type: "mrkdwn",
            text:
              channel.individualGroupMessage + "\n" + channel.coffeeDoneMessage
          }
        }
      ]
    });
  });
};
